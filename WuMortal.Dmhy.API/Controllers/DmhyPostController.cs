﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WuMortal.Dmhy.API.Extensions;
using WuMortal.Dmhy.API.Models;
using WuMortal.Dmhy.Methods;
using WuMortal.Dmhy.Models;

namespace WuMortal.Dmhy.API.Controllers
{
    [Authorize]
    [EnableCors("AllowSpecificOrigin")]
    [Route("api/[controller]")]
    [ApiController]
    [ResponseCache(Duration = 5 * 60)]
    public class DmhyPostController : ControllerBase
    {
        private DmhyPost _postService { get; set; }

        /// <summary>
        /// 获取最新的番剧帖子
        /// </summary>
        /// <param name="currentPage">当前页</param>
        /// <returns></returns>
        [HttpGet("Topics/{currentPage?}")]
        [ResourceCache(Duration = 20 * 60)]
        public async Task<Result> Topics(long currentPage = 1)
        {
            using (HttpClient client = new HttpClient())
            {
                _postService = new DmhyPost(client);


                DPost[] posts = await _postService.GetTopsDataByPageIndexAsync(currentPage);

                if (posts == null)
                {
                    return new Result
                    {
                        Status = 2,
                        ErrorMsg = "没有获取到数据",
                        Title = "最新番剧",
                    };
                }

                return new Result
                {
                    Data = posts,
                    Count = posts.Count(),
                    Status = 1,
                    Title = "最新番剧"
                };
            }
        }

        /// <summary>
        /// 搜索番剧
        /// </summary>
        /// <param name="keyWord">番剧名称</param>
        /// <param name="currentPage">当前页</param>
        /// <returns></returns>
        [HttpGet("Search/{currentPage?}")]
        public async Task<Result> Search(string keyWord, long currentPage = 1)
        {
            if (string.IsNullOrWhiteSpace(keyWord))
            {
                return new Result
                {
                    Status = 2,
                    ErrorMsg = "需要的搜索的番剧名称不能为空",
                    Title = "搜索番剧",
                };
            }

            using (HttpClient client = new HttpClient())
            {
                _postService = new DmhyPost(client);


                DPost[] posts = await _postService.GetTopsDataByKeyWordAsync(keyWord, currentPage);

                if (posts == null)
                {
                    return new Result
                    {
                        Status = 2,
                        ErrorMsg = $"没有搜索：{keyWord}番剧",
                        Title = $"搜索{keyWord}番剧",
                    };
                }

                return new Result
                {
                    Data = posts,
                    Count = posts.Count(),
                    Status = 1,
                    Title = $"搜索「{keyWord}」番剧"
                };
            }
        }

        /// <summary>
        /// 搜索字幕组的帖子
        /// </summary>
        /// <param name="teamId">字幕组Id</param>
        /// <param name="teamName">字幕组名称</param>
        /// <param name="currentPage">当前页</param>
        /// <returns></returns>
        [HttpGet("Team/{currentPage?}")]
        public async Task<Result> Team(long teamId, string teamName, long currentPage = 1)
        {
            using (HttpClient client = new HttpClient())
            {
                _postService = new DmhyPost(client);


                DPost[] posts = await _postService.GetTopsDataByTeamIdAsync(teamId, currentPage);

                if (posts == null)
                {
                    return new Result
                    {
                        Status = 2,
                        ErrorMsg = $"没有搜索：{teamName}字幕组发布的番剧",
                        Title = $"搜索「{teamName}」字幕组发布的番剧",
                    };
                }

                return new Result
                {
                    Data = posts,
                    Count = posts.Count(),
                    Status = 1,
                    Title = $"搜索「{teamName}」字幕组发布的番剧"
                };
            }
        }

        /// <summary>
        /// 搜索用户发布的帖子
        /// </summary>
        /// <param name="userId">用户Id</param>
        /// <param name="userName">用户名称</param>
        /// <param name="currentPage">当前页</param>
        /// <returns></returns>
        [HttpGet("User/{currentPage?}")]
        public async Task<Result> UserPost(long userId, string userName, long currentPage = 1)
        {
            using (HttpClient client = new HttpClient())
            {
                _postService = new DmhyPost(client);


                DPost[] posts = await _postService.GetTopsDataByUserIdAsync(userId, currentPage);

                if (posts == null)
                {
                    return new Result
                    {
                        Status = 2,
                        ErrorMsg = $"没有搜索：{userName}用户发布的番剧",
                        Title = $"搜索「{userName}」用户发布的番剧",
                    };
                }

                return new Result
                {
                    Data = posts,
                    Count = posts.Count(),
                    Status = 1,
                    Title = $"搜索「{userName}」用户发布的番剧"
                };
            }
        }

        /// <summary>
        /// 根据分类Id获取帖子
        /// </summary>
        /// <param name="categoryId">类型Id</param>
        /// <param name="categoryName">类型名称</param>
        /// <param name="currentPage">当前页</param>
        /// <returns></returns>
        [HttpGet("Category/{currentPage?}")]
        public async Task<Result> Category(long categoryId, string categoryName, long currentPage = 1)
        {
            using (HttpClient client = new HttpClient())
            {
                _postService = new DmhyPost(client);


                DPost[] posts = await _postService.GetTopsDataByCategoryIdAsync(categoryId, currentPage);

                if (posts == null)
                {
                    return new Result
                    {
                        Status = 2,
                        ErrorMsg = $"没有搜索：{categoryName}的番剧",
                        Title = $"搜索「{categoryName}」的番剧",
                    };
                }

                return new Result
                {
                    Data = posts,
                    Count = posts.Count(),
                    Status = 1,
                    Title = $"搜索「{categoryName}」的番剧"
                };
            }
        }
    }
}