﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WuMortal.Dmhy.API.Extensions;
using WuMortal.Dmhy.API.Models;
using WuMortal.Dmhy.Methods;
using WuMortal.Dmhy.Models;

namespace WuMortal.Dmhy.API.Controllers
{
    [Authorize]
    [EnableCors("AllowSpecificOrigin")]
    [Route("api/[controller]")]
    [ApiController]
    [ResponseCache(Duration = 60)]
    public class DmhyPostDetailedController : ControllerBase
    {
        private DmhyPostDetailed _postDetailedServer;

        /// <summary>
        /// 获取帖子详细内容
        /// </summary>
        /// <param name="postId">贴纸编号</param>
        /// <returns></returns>
        [HttpGet("Content/{postId}")]
        public async Task<Result> PostContent(string postId)
        {
            if (string.IsNullOrWhiteSpace(postId))
            {
                return new Result
                {
                    Status = 2,
                    ErrorMsg = "番剧地址不能不能为空",
                    Title = "搜索番剧",
                };
            }

            using (HttpClient client = new HttpClient())
            {
                _postDetailedServer = new DmhyPostDetailed(client);

                DPostDetailed detailed = await _postDetailedServer.GetPostDetailedAsync(postId);

                if (detailed == null)
                {
                    return new Result
                    {
                        Status = 2,
                        ErrorMsg = "没有获取到数据",
                        Title = "番剧详细",
                    };
                }

                return new Result
                {
                    Data = detailed,
                    Status = 1,
                    Title = $"番剧「{postId}」详细"
                };
            }
        }

        /// <summary>
        /// 获取帖子评论
        /// </summary>
        /// <param name="postId">帖子Id</param>
        /// <returns></returns>
        [HttpGet("Comments/{postId}")]
        public async Task<Result> PostComments(string postId)
        {
            if (string.IsNullOrWhiteSpace(postId))
            {
                return new Result
                {
                    Status = 2,
                    ErrorMsg = "番剧地址不能不能为空",
                    Title = "搜索番剧",
                };
            }

            using (HttpClient client = new HttpClient())
            {
                _postDetailedServer = new DmhyPostDetailed(client);

                DComment[] comments = await _postDetailedServer.GetCommentsAsync(postId);

                if (comments == null)
                {
                    return new Result
                    {
                        Status = 2,
                        ErrorMsg = "暂无评论",
                        Title = "番剧评论",
                    };
                }

                return new Result
                {
                    Data = comments,
                    Status = 1,
                    Title = $"番剧「{postId}」的评论"
                };
            }
        }
    }
}
