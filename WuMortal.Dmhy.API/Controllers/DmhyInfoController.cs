﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WuMortal.Dmhy.API.Extensions;
using WuMortal.Dmhy.API.Models;
using WuMortal.Dmhy.Methods;
using WuMortal.Dmhy.Models;

namespace WuMortal.Dmhy.API.Controllers
{
    [Authorize]
    [EnableCors("AllowSpecificOrigin")]
    [Route("api/[controller]")]
    [ApiController]
    public class DmhyInfoController : ControllerBase
    {
        private DmhyInfo _infoService;

        /// <summary>
        /// 番剧索引信息
        /// </summary>
        /// <returns></returns>
        [ResponseCache(Duration = 2 * 24 * 3600)]
        [ResourceCache(Duration = 2 * 24 * 3600)]
        [HttpGet("Programme")]
        public async Task<Result> Programme()
        {
            using (HttpClient client = new HttpClient())
            {
                _infoService = new DmhyInfo(client);

                var data = await _infoService.GetDramaIndexDataAsync();

                if (data == null)
                {
                    return new Result
                    {
                        Status = 2,
                        ErrorMsg = "没有获取到数据",
                        Title = "动漫花园-番剧索引",
                    };
                }

                return new Result
                {
                    Data = data,
                    Status = 1,
                    Count = data.Count(),
                    Title = "动漫花园-番剧索引"
                };
            }
        }

        [ResponseCache(Duration = 7 * 24 * 3600)]
        [ResourceCache(Duration = 7 * 24 * 3600)]
        /// <summary>
        /// 获取动漫花园所有分类
        /// </summary>
        /// <returns></returns>
        [HttpGet("Categoies")]
        public async Task<Result> Categoies()
        {
            using (HttpClient client = new HttpClient())
            {
                _infoService = new DmhyInfo(client);
                DCategory[] categories = await _infoService.GetDramaCategoryAsync();


                if (categories == null)
                {
                    return new Result
                    {
                        Status = 2,
                        ErrorMsg = "没有获取到数据",
                        Title = "动漫花园-分类",
                    };
                }

                return new Result
                {
                    Data = categories,
                    Status = 1,
                    Count = categories.Count(),
                    Title = "动漫花园-分类"
                };
            }
        }

        [ResponseCache(Duration = 7 * 24 * 3600)]
        [ResourceCache(Duration = 7 * 24 * 3600)]
        /// <summary>
        /// 获取所有字幕组信息
        /// </summary>
        /// <returns></returns>
        [HttpGet("Teams")]
        public async Task<Result> Teams()
        {
            using (HttpClient client = new HttpClient())
            {
                _infoService = new DmhyInfo(client);
                DTeam[] teams = await _infoService.GetTeamAsync();


                if (teams == null)
                {
                    return new Result
                    {
                        Status = 2,
                        ErrorMsg = "没有获取到数据",
                        Title = "动漫花园-字幕组",
                    };
                }

                return new Result
                {
                    Data = teams,
                    Status = 1,
                    Count = teams.Count(),
                    Title = "动漫花园-字幕组"
                };
            }
        }

        [ResponseCache(Duration = 24 * 3600)]
        [ResourceCache(Duration = 24 * 3600)]
        /// <summary>
        /// 热门资源信息
        /// </summary>
        /// <returns></returns>
        [HttpGet("HotPosts")]
        public async Task<Result> HotPosts()
        {
            using (HttpClient client = new HttpClient())
            {
                _infoService = new DmhyInfo(client);
                DHotPost[] hotPosts = await _infoService.GetHotPostAsync();


                if (hotPosts == null)
                {
                    return new Result
                    {
                        Status = 2,
                        ErrorMsg = "没有获取到数据",
                        Title = "动漫花园-热门资源",
                    };
                }

                return new Result
                {
                    Data = hotPosts,
                    Status = 1,
                    Count = hotPosts.Count(),
                    Title = "动漫花园-热门资源"
                };
            }
        }
    }
}