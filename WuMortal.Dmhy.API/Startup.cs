﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Exceptionless;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.PlatformAbstractions;
using NLog.Extensions.Logging;
using NLog.Web;
using Swashbuckle.AspNetCore.Swagger;
using WuMortal.Dmhy.API.Extensions;
using WuMortal.Dmhy.Methods;
using WuMortal.Dmhy.Service;

namespace WuMortal.Dmhy.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            var domains = Configuration.GetSection("CORS:DoMain").Value.Split(",");
            services.AddCors(options =>
            {
                options.AddPolicy("AllowSpecificOrigin", builder =>
                {
                    builder.WithOrigins(domains)
                    .AllowAnyHeader()
                    .AllowAnyMethod();
                });
            });

            services.AddMemoryCache();

            //IdentityServer
            services.AddAuthentication("Bearer")
            .AddIdentityServerAuthentication(options =>
            {
                options.ApiName = "DmhyAPI";
                options.Authority = Configuration.GetSection("IdentityServer:Authority").Value;//identity server 地址
                options.RequireHttpsMetadata = false;
            });

            //注入过滤器
            services.AddSingleton(typeof(ResourceCacheAttribute));
            services.AddSingleton(typeof(DmhyExceptionFilter));
            services.AddSingleton(typeof(AuthorizationFilter));
            services.AddMvc(option =>
            {
                var serviceProvider = services.BuildServiceProvider();
                var exceptionFilter = serviceProvider.GetService<DmhyExceptionFilter>();
                var authorizationFilter = serviceProvider.GetService<AuthorizationFilter>();
                option.Filters.Add(exceptionFilter);
                option.Filters.Add(authorizationFilter);
            });


            Assembly asm = Assembly.Load(new AssemblyName("WuMortal.Dmhy.Service"));
            var serviceTypes = asm.GetTypes()
                .Where(t => typeof(IServiceTag).IsAssignableFrom(t) && !t.GetTypeInfo().IsAbstract);
            foreach (var serviceType in serviceTypes)
            {
                services.AddSingleton(serviceType);
            }

            //配置API帮助页接口信息
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info
                {
                    Version = "v1",
                    Title = "动漫花园「Dmhy」接口文档",
                    Description = "RESTful API for Dmhy",
                    TermsOfService = "None",
                    Contact = new Contact { Name = "Mortal_Wu", Email = "1170621609@qq.com", Url = "" }
                });

                //Set the comments path for the swagger json and ui.
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var xmlPath = Path.Combine(basePath, "dmhyapi.xml");
                c.IncludeXmlComments(xmlPath);

            });



        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            app.UseCors("AllowSpecificOrigin");

            app.UseAuthentication();

            //向控制台输入日志
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            //配置Exceptionless 日志管理中心
            string apiKey = Configuration.GetSection("Exceptionless:ApiKey").Value;
            app.UseExceptionless(apiKey);

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS etc.), specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Dmhy API V1");
            });

            app.UseMvc();
        }
    }
}
