﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WuMortal.Dmhy.API.Models;
using WuMortal.Dmhy.Service;

namespace WuMortal.Dmhy.API.Extensions
{
    public class AuthorizationFilter : IAsyncAuthorizationFilter
    {
        private UserService _userService;
        private WebAPIService _webAPIService;

        public AuthorizationFilter(UserService userService, WebAPIService webAPIService)
        {
            _userService = userService;
            _webAPIService = webAPIService;
        }

        public async Task OnAuthorizationAsync(AuthorizationFilterContext context)
        {
            //string email = context.HttpContext.User.FindFirst("Email").Value;
            #region 获取用户
            var userId = context.HttpContext.User.FindFirst("UserId");

            if (userId == null)
            {
                return;
            }

            string id = userId.Value;

            var user = await _userService.GetUserAsync(long.Parse(id));
            if (user == null)
            {
                context.Result = new JsonResult(new Result
                {
                    Status = 3,
                    ErrorMsg = "账号异常，请联系管理员！"
                });
                return;
            }
            #endregion

            //判断API是否Open

            var userApis = await _webAPIService.GetWebAPIByUserIdAsync(user.Id);

            //获取控制器和action名称
            var routes = context.RouteData.Values.Values.ToArray();
            string[] routeInfo = new string[2];
            string apiName = null;
            for (int i = routes.Length; i > 0; i--)
            {
                if (i > routes.Length - 2)
                {
                    routeInfo[routes.Length - i] = (string)routes[i - 1];
                }
            }

            apiName = string.Join('-', routeInfo);

            apiName = apiName.Replace("Dmhy", "", StringComparison.OrdinalIgnoreCase);

            var webApi = await _webAPIService.GetWebAPIAsync(apiName);

            if (webApi == null)
            {
                context.Result = new JsonResult(new Result
                {
                    Status = 5,
                    ErrorMsg = "接口异常！"
                });
                return;
            }

            if (webApi.IsDeleted)
            {
                context.Result = new JsonResult(new Result
                {
                    Status = 4,
                    ErrorMsg = "接口已关闭！"
                });
                return;
            }

            bool exist = userApis.Any(u => u.IsDeleted == false && u.Name == apiName);

            if (!exist)
            {
                context.Result = new JsonResult(new Result
                {
                    Status = 4,
                    ErrorMsg = "没有权限！"
                });
                return;
            }

        }
    }
}
