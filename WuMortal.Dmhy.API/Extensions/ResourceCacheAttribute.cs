﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WuMortal.Dmhy.API.Models;

namespace WuMortal.Dmhy.API.Extensions
{
    /// <summary>
    /// 后台缓存
    /// </summary>
    [AttributeUsage(AttributeTargets.All, AllowMultiple = false)]
    public class ResourceCacheAttribute : ActionFilterAttribute
    {
        private IMemoryCache _cache;

        /// <summary>
        /// 缓存时间
        /// </summary>
        public int Duration { get; set; }

        public ResourceCacheAttribute()
        {
            _cache = new MemoryCache(new MemoryCacheOptions());
        }

        public override void OnResultExecuted(ResultExecutedContext context)
        {
            var routes = context.RouteData.Values.Values.ToArray();

            string controllerName = routes[1].ToString();
            string actionName = routes[0].ToString();

            if (_cache.TryGetValue($"{controllerName}_{actionName}", out object value))
            {
                return;
            }

            _cache.Set($"{controllerName}_{actionName}", context.Result, TimeSpan.FromSeconds(Duration));

        }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            var routes = context.RouteData.Values.Values.ToArray();
            string controllerName = routes[1].ToString();
            string actionName = routes[0].ToString();

            ObjectResult value;

            if (_cache.TryGetValue($"{controllerName}_{actionName}", out value))
            {
                context.Result = new JsonResult((Result)value.Value);
                return;
            }

        }
    }
}
