﻿using Exceptionless;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WuMortal.Dmhy.API.Extensions
{
    public class DmhyExceptionFilter : IExceptionFilter
    {
        private ILogger _logger;

        public DmhyExceptionFilter(ILoggerFactory logFactory)
        {
            //注入日志服务
            _logger = logFactory.CreateLogger(typeof(DmhyExceptionFilter));
        }

        public void OnException(ExceptionContext context)
        {
            _logger.LogError("程序发生异常:" + context.Exception.Message);
        }
    }
}
