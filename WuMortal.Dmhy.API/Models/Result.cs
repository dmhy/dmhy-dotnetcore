﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WuMortal.Dmhy.API.Models
{
    public class Result
    {
        /// <summary>
        /// 执行结果 1 成功 2没有数据 3出现错误
        /// </summary>
        public int Status { get; set; }

        /// <summary>
        /// 错误消息
        /// </summary>
        public string ErrorMsg { get; set; }

        /// <summary>
        /// 数据总条数
        /// </summary>
        public int Count { get; set; }


        /// <summary>
        /// 标题
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 执行返回数据
        /// </summary>
        public object Data { get; set; }
    }
}
