﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace WuMortal.Dmhy.Client.Controllers
{
    [EnableCors("AllowSpecificOrigin")]
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private IConfiguration _configuration;

        public LoginController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        [HttpPost]
        public async Task<ActionResult> RequestToken()
        {
            string address = _configuration.GetSection("IdentityServer:Authority").Value;
            string secretKey = _configuration.GetSection("SecretKey").Value;
            string userEmail = _configuration.GetSection("UserInfo:Email").Value;
            string password = _configuration.GetSection("UserInfo:Password").Value;

            Dictionary<string, string> dict = new Dictionary<string, string>();
            dict["client_id"] = "Dmhy_WebAPP";
            dict["client_secret"] = secretKey;
            dict["grant_type"] = "password";
            dict["UserName"] = userEmail;
            dict["Password"] = password;

            using (HttpClient httpClient = new HttpClient())
            using (var content = new FormUrlEncodedContent(dict))
            {
                var result = await httpClient.PostAsync($"{address}/connect/token", content);
                if (result.IsSuccessStatusCode)
                {
                    Response.ContentType = "application/json";
                    return Content(await result.Content.ReadAsStringAsync());
                }
                else
                {
                    return StatusCode((int)result.StatusCode);
                }
            }
        }
    }
}
