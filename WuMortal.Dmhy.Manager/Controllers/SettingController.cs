﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WuMortal.Dmhy.Manager.Extensions;
using WuMortal.Dmhy.Service;

namespace WuMortal.Dmhy.Manager.Controllers
{
    [Authorize]
    //[CheckLogin]
    public class SettingController : Controller
    {
        private AdminUserService _userService;

        public SettingController(AdminUserService userService)
        {
            _userService = userService;
        }

        public async Task<IActionResult> Index()
        {
            var result = HttpContext.GetUserId();
            if (result == null)
            {
                await HttpContext.SignOutAsync($"{nameof(WuMortal.Dmhy)}AuthenticationScheme");
                return Redirect("/User/Login");
            }

            var id = (long)result;

            //var id = int.Parse(HttpContext.Session.GetString("LOGINTAG"));
            var user = await _userService.GetAdminUserAsync(id);

            if (user.EmailAddress != HttpContext.GetUserEmail())
            {
                await HttpContext.SignOutAsync($"{nameof(WuMortal.Dmhy)}AuthenticationScheme");

                return Redirect("/User/Login");
            }

            return View(user);
        }

        public IActionResult History()
        {
            return View();
        }

        public IActionResult About()
        {
            return View();
        }
    }
}