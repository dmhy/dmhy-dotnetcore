﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Security.Claims;
using System.Threading.Tasks;
using WuMortal.Dmhy.Service;

namespace WuMortal.Dmhy.Manager.Controllers
{
    public class UserController : Controller
    {
        private AdminUserService _adminUserService;

        public UserController(AdminUserService adminUserService)
        {
            _adminUserService = adminUserService;
        }

        [HttpGet]
        public IActionResult Login()
        {
            var result = HttpContext.GetUserId();

            if (result != null)
            {
                return Redirect("/");
            }

            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Login(string email, string pwd)
        {


            if (string.IsNullOrWhiteSpace(email) || string.IsNullOrWhiteSpace(pwd))
            {
                return Json(new
                {
                    status = 1,
                    msg = "email or password is empty!"
                });
            }

            bool isLogin = await _adminUserService.CheckAdminUserAsync(email, pwd);


            if (!isLogin)
            {
                return Json(new
                {
                    status = 1,
                    msg = "email or password error!"
                });
            }

            var adminUser = await _adminUserService.GetAdminUserAsync(email);

            //将信息存入cookie中
            var user = new ClaimsPrincipal(
              new ClaimsIdentity(new[]
              {
                    new Claim("Email", adminUser.EmailAddress),
                    new Claim("Id", adminUser.Id.ToString()),
              },
            $"{nameof(WuMortal.Dmhy)}AuthenticationScheme"));
            await HttpContext.SignInAsync($"{nameof(WuMortal.Dmhy)}AuthenticationScheme", user, new AuthenticationProperties
            {
                IsPersistent = true,
                ExpiresUtc = DateTimeOffset.Now.Add(TimeSpan.FromDays(7)) // 有效时间
            });

            return Json(new
            {
                status = 0
            });
        }

        [HttpPost]
        public async Task<IActionResult> Logout()
        {
            //HttpContext.Session.Clear();

            await HttpContext.SignOutAsync($"{nameof(WuMortal.Dmhy)}AuthenticationScheme");

            return Json(new
            {
                status = 0
            });
        }
    }
}