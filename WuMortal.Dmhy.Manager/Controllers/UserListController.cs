﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WuMortal.Dmhy.Manager.Extensions;
using WuMortal.Dmhy.Manager.Models;
using WuMortal.Dmhy.Service;

namespace WuMortal.Dmhy.Manager.Controllers
{
    [Authorize]
    //[CheckLogin]
    public class UserListController : Controller
    {
        private UserService _userService;

        private WebAPIService _webAPIService;

        public UserListController(UserService userService, WebAPIService webAPIService)
        {
            _userService = userService;
            _webAPIService = webAPIService;
        }

        public async Task<IActionResult> Index()
        {
            var users = await _userService.GetAllUserAsync();

            return View(users);
        }

        public async Task<IActionResult> UserInfo(long id)
        {
            var user = await _userService.GetUserAsync(id);

            if (user == null)
            {
                return Content("user not found !");
            }

            //var apis = await _webAPIService.GetWebAPIByUserIdAsync(1);

            return View(user);
        }

        [HttpPost]
        public async Task<IActionResult> UserAPIs(long id)
        {
            var apis = await _webAPIService.GetWebAPIByUserIdAsync(id);

            return Json(new
            {
                status = 0,
                data = apis
            });
        }

        [HttpPost]
        public async Task<IActionResult> ChangeUserAPIStatus(long userId, long apiId, bool status)
        {
            await _webAPIService.SetUserWebAPIStatusAsync(apiId, userId, status);

            return Json(new
            {
                status = 0
            });
        }

        [HttpPost]
        public async Task<IActionResult> WebAPIs()
        {
            var apis = await _webAPIService.GetAllWebAPIsAsync();

            string[] values = new string[apis.Length];

            for (int i = 0; i < apis.Length; i++)
            {
                values[i] = $"{apis[i].Id}-{apis[i].Name}";
            }

            return Json(new
            {
                status = 0,
                data = values
            });
        }

        [HttpPost]
        public async Task<IActionResult> AddUserAPI(long userId, long apiId)
        {
            if (userId <= 0 || apiId <= 0)
            {
                return Json(new
                {
                    status = 1,
                    msg = "api exist"
                });
            }

            var apis = await _webAPIService.GetWebAPIByUserIdAsync(userId);

            var api = await _webAPIService.GetWebAPIAsync(apiId);

            if (apis.Any(a => a.Name == api.Name))
            {
                return Json(new
                {
                    status = 1,
                    msg = "api exist"
                });
            }


            await _webAPIService.AddUserAPIAsync(userId, new long[] { apiId });

            return Json(new
            {
                status = 0
            });
        }
    }
}
