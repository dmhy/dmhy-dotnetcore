﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WuMortal.Dmhy.Manager.Extensions;
using WuMortal.Dmhy.Manager.Models;
using WuMortal.Dmhy.Service;

namespace WuMortal.Dmhy.Manager.Controllers
{
    [Authorize]
    //[CheckLogin]
    public class HomeController : Controller
    {
        private WebAPIService _webAPIService;

        public HomeController(WebAPIService webAPIService)
        {
            _webAPIService = webAPIService;
        }

        public async Task<IActionResult> Index()
        {
            var apis = await _webAPIService.GetAllWebAPIsAsync();

            return View(apis);
        }

        [HttpPost]
        public async Task<IActionResult> AddNewAPI(AddAPIModel model)
        {
            if (string.IsNullOrWhiteSpace(model.Name) || string.IsNullOrWhiteSpace(model.Desc))
            {
                return Json(new
                {
                    status = 1,
                    msg = "name or desc is Empty"
                });
            }

            var api = await _webAPIService.GetWebAPIAsync(model.Name);

            if (api != null)
            {
                return Json(new
                {
                    status = 1,
                    msg = $"{model.Name} API exist"
                });
            }

            long id = await _webAPIService.AddAPIAsync(model.Name.Trim(), model.Desc.Trim());


            return Json(new
            {
                status = 0,
            });

        }

        [HttpPost]
        public async Task<IActionResult> ChangeAPIStatus(long id, bool status)
        {
            await _webAPIService.SetWebAPIStatusAsync(id, !status);

            return Json(new
            {
                status = 0,
            });
        }
    }
}