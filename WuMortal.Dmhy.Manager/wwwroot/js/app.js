// Dom7
var $$ = Dom7;

// Framework7 App main instance
var app = new Framework7({
    root: '#app', // App root element
    id: 'io.framework7.testapp', // App bundle ID
    name: 'DmhyManager', // App name
    theme: 'auto', // Automatic theme detection
    // App root data
    data: function () {
        return {
            user: {
                firstName: 'John',
                lastName: 'Doe',
            },
            // Demo products for userList section
            users: [
                {
                    id: '1',
                    title: 'Wu Meng',
                    description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nisi tempora similique reiciendis, error nesciunt vero, blanditiis pariatur dolor, minima sed sapiente rerum, dolorem corrupti hic modi praesentium unde saepe perspiciatis.'
                },
                {
                    id: '2',
                    title: 'Wu Mortal',
                    description: 'Velit odit autem modi saepe ratione totam minus, aperiam, labore quia provident temporibus quasi est ut aliquid blanditiis beatae suscipit odio vel! Nostrum porro sunt sint eveniet maiores, dolorem itaque!'
                },
                {
                    id: '3',
                    title: 'Mortal Wu',
                    description: 'Expedita sequi perferendis quod illum pariatur aliquam, alias laboriosam! Vero blanditiis placeat, mollitia necessitatibus reprehenderit. Labore dolores amet quos, accusamus earum asperiores officiis assumenda optio architecto quia neque, quae eum.'
                },
            ]
        };
    },
    // App root methods
    methods: {

    },
    // App routes
    routes: routes,
});

// Init/Create views
var homeView = app.views.create('#view-home', {
    url: '/'
});
var userListView = app.views.create('#view-userList', {
    url: '/userList/'
});
var settingsView = app.views.create('#view-settings', {
    url: '/settings/'
});

$$('.toggle').on('change', function (e) {
    var toggle = app.toggle.get(this);
    var id = $$(this).find('input').val();
    var status = toggle.checked;

    app.preloader.show();

    $.ajax({
        type: "post",
        dataType: "json",
        url: "/home/ChangeAPIStatus",
        data: { "id": id, "status": status },
        success: function (data) {

            app.preloader.hide();

            if (data.status == 1) {
                showMessage(data.msg);
                return;
            }
            else if (data.status == 0) {
                showMessage('api status change ok!');
            } else {
                showMessage('ERROR');
                return;
            }

        },
        error: function () {
            showMessage('ERROR');
        }
    });

});

// add api
$$('#addAPI').on('click', function () {
    var $apiNameEle = $$('#apiName');
    var $apiDescribe = $$('#apiDescribe');

    app.preloader.show();
    $.ajax({
        type: "post",
        dataType: "json",
        url: "/home/addNewAPI",
        data: { "Name": $apiNameEle.val(), "Desc": $apiDescribe.val() },
        success: function (data) {
            app.preloader.hide();
            if (data.status == 1) {
                showMessage(data.msg);
                return;
            }
            else if (data.status == 0) {
                app.dialog.alert('Add API OK !', 'Message');

                //Clear input content
                $apiNameEle.val('');
                $apiDescribe.val('');
                app.input.checkEmptyState($apiNameEle);
                app.input.checkEmptyState($apiDescribe);
            } else {
                showMessage('ERROR');
                return;
            }

        },
        error: function () {
            showMessage('ERROR');
        }
    });


});


function showMessage(text, time = 2000) {
    // Create top toast
    var toastTop = app.toast.create({
        text: text,
        position: 'top',
        closeTimeout: time,
    });
    toastTop.open();
}