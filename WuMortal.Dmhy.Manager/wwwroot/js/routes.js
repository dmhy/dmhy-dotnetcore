routes = [
    {
        path: '/',
        url: '/home/index'
    },
    {
        path: '/about/',
        url: './setting/about'
    },
    {
        path: '/userList/',
        url: 'userList/index'
    },
    {
        path: '/user/:id/',
        async: function (routeTo, routeFrom, resolve, reject) {
            // Router instance
            var router = this;

            // App instance
            var app = router.app;

            // Show Preloader
            app.preloader.show();
            // User ID from request
            var userId = routeTo.params.id;


            var userAPIData;

            $.ajax({
                type: "post",
                dataType: "json",
                url: "/userList/UserAPIs",
                data: { "id": userId },
                success: function (data) {

                    app.preloader.hide();

                    if (data.status == 1) {
                        showMessage(data.msg);
                        return;
                    }
                    else if (data.status == 0) {
                        userAPIData = data.data;
                        resolve(
                            {
                                componentUrl: 'userList/userInfo/' + userId
                            },
                            {
                                context: {
                                    apis: userAPIData
                                }
                            }
                        );

                    } else {
                        showMessage('ERROR');
                        return;
                    }

                },
                error: function () {
                    showMessage('ERROR');
                }
            });




        }
    },
    {
        path: '/settings/',
        url: '/setting/index'
    },
    {
        path: '/history/',
        url: './setting/history'
    },
    // Default route (404 page). MUST BE THE LAST
    {
        path: '(.*)',
        url: './pages/404.html'
    }
];
