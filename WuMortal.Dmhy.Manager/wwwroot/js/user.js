$$(document).on('page:init', '.page[data-name="userInfo"]', function (page) {
    app.preloader.show();

    var pickerDevice = showAPIPicker(page);

    app.preloader.hide();

    $$('.userAPIStatus').on('click', function () {
        var toggle = app.toggle.get(this);
        var id = $$(this).find('input').val();
        var status = toggle.checked;
        var userId = $$('#userInfoId').val();

        app.preloader.show();

        $.ajax({
            type: "post",
            dataType: "json",
            url: "/userList/ChangeUserAPIStatus",
            data: { "userId": userId, "apiId": id, "status": status },
            success: function (data) {

                app.preloader.hide();

                if (data.status == 1) {
                    showMessage(data.msg);
                    return;
                }
                else if (data.status == 0) {
                    showMessage('api status change ok!');
                } else {
                    showMessage('ERROR');
                    return;
                }

            },
            error: function () {
                showMessage('ERROR');
            }
        });
    });
});

function showAPIPicker(page) {
    loadAPIs(function (apis) {
        pickerDevice = app.picker.create({
            inputEl: '#addUserAPIName',
            cols: [
                {
                    textAlign: 'center',
                    values: apis
                }
            ]
        });

        $$('#addUserAPI').on('click', function () {
            var apiName = $$('#addUserAPIName').val();

            if (!apiName) {
                showMessage('Please select first, then click \'Add API\'. !', 3000);
                pickerDevice.open();
                return;
            }

            var id = apiName.split('-')[0];

            addUserAPI(id, page);


        });
    });
}

function addUserAPI(id,page) {
    var userId = $$('#userInfoId').val();
    var apiId = id;

    $.ajax({
        type: "post",
        dataType: "json",
        url: "/userList/addUserApi",
        data: { userId: userId, apiId: apiId },
        success: function (data) {
            if (data.status == 1) {
                showMessage(data.msg);
                return;
            }
            else if (data.status == 0) {
                page.detail.router.back('/userList');
                showMessage('add api ok');

            } else {
                showMessage('ERROR');
                return;
            }

        },
        error: function () {
            showMessage('ERROR');
        }
    });
}

function loadAPIs(action) {
    $.ajax({
        type: "post",
        dataType: "json",
        url: "/userList/WebAPIs",
        success: function (data) {
            if (data.status == 1) {
                showMessage(data.msg);
                return;
            }
            else if (data.status == 0) {
                var apis = data.data;
                action(apis);
            } else {
                showMessage('ERROR');
                return;
            }

        },
        error: function () {
            showMessage('ERROR');
        }
    });
}
