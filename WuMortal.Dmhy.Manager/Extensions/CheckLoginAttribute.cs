﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WuMortal.Dmhy.Manager.Extensions
{
    public class CheckLoginAttribute : ResultFilterAttribute
    {
        public string Name { get; set; }

        public CheckLoginAttribute()
        {
        }

        public CheckLoginAttribute(string name)
        {
            Name = name;
        }

        public override void OnResultExecuting(ResultExecutingContext context)
        {
            var loginTag = context.HttpContext.Session.GetString("LOGINTAG");
            if (loginTag == null)
            {
                if (context.HttpContext.Request.IsAjax())
                {
                    context.Result = new JsonResult(
                        new
                        {
                            status = "redirect",
                            msg = "not login!",
                            Data = "/user/login"
                        });

                    return;
                }

                context.Result = new RedirectResult("/user/login");
                return;
            }
        }


    }
}
