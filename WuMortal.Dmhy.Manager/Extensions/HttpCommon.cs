﻿using Microsoft.AspNetCore.Http;
using System;
using System.Linq;

namespace WuMortal.Dmhy.Manager
{
    public static class HttpCommon
    {
        public static bool IsAjax(this HttpRequest req)
        {
            bool result = false;

            var xreq = req.Headers.ContainsKey("x-requested-with");
            if (xreq)
            {
                result = req.Headers["x-requested-with"] == "XMLHttpRequest";
            }

            return result;
        }

        //从cookie获取用户信息
        public static long? GetUserId(this HttpContext context)
        {
            var identities = context.User.Identities.Single();

            if (identities == null)
            {
                return null;
            }

            var value = identities.Claims.SingleOrDefault(c => c.Type == "Id");

            if (value == null)
            {
                return null;
            }

            long id = Convert.ToInt64(value.Value);

            return id;
        }

        public static string GetUserEmail(this HttpContext context)
        {
            var identities = context.User.Identities.Single();

            if (identities == null)
            {
                return null;
            }

            var value = identities.Claims.SingleOrDefault(c => c.Type == "Email");

            if (value == null)
            {
                return null;
            }

            string email = value.Value;

            return email;
        }
    }
}
