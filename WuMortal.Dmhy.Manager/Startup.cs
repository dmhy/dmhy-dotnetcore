﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using WuMortal.Dmhy.Manager.Extensions;
using WuMortal.Dmhy.Service;

namespace WuMortal.Dmhy.Manager
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMemoryCache();
            services.AddDistributedMemoryCache();
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });
            #region Cookie

            services.AddAuthentication($"{nameof(WuMortal.Dmhy)}AuthenticationScheme")
                    .AddCookie($"{nameof(WuMortal.Dmhy)}AuthenticationScheme", options =>
                    {
                        options.AccessDeniedPath = "/User/Login/";
                        options.LoginPath = "/User/Login/";
                    }); 
            #endregion

            services.AddSession(options =>
            {
                //解决方案是将会话cookie标记为必要。 不标记将Session将会读取不到
                //指示此cookie是否对应用程序正常运行至关重要。如果为真，则可以绕过同意政策检查。默认值为false。
                //这将保持cookie策略选项不变，并且会话仍然按预期工作，因为CookiePolicyOptions.CheckConsentNeeded只影响非必要的cookie。
                options.Cookie.IsEssential = true;
                options.IdleTimeout = TimeSpan.FromSeconds(1200);
                options.Cookie.HttpOnly = true;
            });

            //注入所有的Service
            Assembly asm = Assembly.Load(new AssemblyName("WuMortal.Dmhy.Service"));
            var serviceTypes = asm.GetTypes()
                .Where(t => typeof(IServiceTag).IsAssignableFrom(t) && !t.GetTypeInfo().IsAbstract);
            foreach (var serviceType in serviceTypes)
            {
                services.AddSingleton(serviceType);
            }

            //注入过滤器
            services.AddSingleton(typeof(DmhyExceptionFilter));
            services.AddMvc(option =>
            {
                var serviceProvider = services.BuildServiceProvider();
                var filter = serviceProvider.GetService<DmhyExceptionFilter>();

                option.Filters.Add(filter);
            });

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            //向控制台输入日志
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();
                        
            app.UseStaticFiles();
            //Cookie
            app.UseAuthentication();
            app.UseCookiePolicy();
            app.UseSession();


            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
