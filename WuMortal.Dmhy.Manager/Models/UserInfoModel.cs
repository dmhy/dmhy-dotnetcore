﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WuMortal.Dmhy.Service.EntityModel;

namespace WuMortal.Dmhy.Manager.Models
{
    public class UserInfoModel
    {
        public User User { get; set; }

        public WebAPI[] WebAPIs { get; set; } = { };
    }
}
