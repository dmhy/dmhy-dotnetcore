﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace WuMortal.Dmhy.Commons
{
    public class UserHelper
    {
        public static string Md5(string value)
        {
            byte[] bytes;
            using (var md5 = MD5.Create())
            {
                bytes = md5.ComputeHash(Encoding.UTF8.GetBytes(value));
            }
            var result = new StringBuilder();
            foreach (byte t in bytes)
            {
                result.Append(t.ToString("X2"));
            }
            return result.ToString();
        }

        /// <summary>
        /// 获取盐
        /// </summary>
        /// <param name="count">长度</param>
        /// <returns></returns>
        public static string Salt(int length)
        {
            StringBuilder sb = new StringBuilder();

            Random ran = new Random();

            for (int i = 0; i < length; i++)
            {
                int n = ran.Next(33, 127);
                sb.Append((char)n);
            }

            return sb.ToString();
        }
    }
}
