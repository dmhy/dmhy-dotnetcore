﻿using IdentityServer4;
using IdentityServer4.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace WuMortal.Dmhy.IdentityServer
{
    public class Config
    {

        /// <summary>
        /// 返回应用列表
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<ApiResource> GetApiResources()
        {
            List<ApiResource> resources = new List<ApiResource>();
            //ApiResource第一个参数是应用的名字，第二个参数是描述
            resources.Add(new ApiResource("DmhyAPI", "动漫花园API"));
            return resources;
        }

        /// <summary>
        /// 返回账号列表
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<Client> GetClients()
        {            
            var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory())
                               .AddJsonFile("appsettings.json").Build();
            string secretKey = builder.GetSection("SecretKey").Value;

            List<Client> clients = new List<Client>();
            clients.Add(new Client
            {
                AccessTokenLifetime = 3600,//token 有效时间 单位：秒
                ClientId = "Dmhy_WebAPP",//API账号、客户端Id
                AllowedGrantTypes = GrantTypes.ResourceOwnerPassword,
                ClientSecrets =
                {
                    new Secret(secretKey.Sha256())//秘钥
                },
                AllowedScopes = {"DmhyAPI",
                    IdentityServerConstants.StandardScopes.OpenId,
                    IdentityServerConstants.StandardScopes.Profile }//这个账号支持访问哪些应用
            });
            return clients;
        }
    }
}
