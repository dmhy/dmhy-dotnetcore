﻿using IdentityServer4.Models;
using IdentityServer4.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using WuMortal.Dmhy.Service;

namespace WuMortal.Dmhy.IdentityServer
{
    public class ResourceOwnerPasswordValidator : IResourceOwnerPasswordValidator
    {
        private UserService _userService;
        public ResourceOwnerPasswordValidator(UserService userService)
        {
            _userService = userService;
        }

        public async Task ValidateAsync(ResourceOwnerPasswordValidationContext context)
        {
            if (string.IsNullOrWhiteSpace(context.UserName) || string.IsNullOrWhiteSpace(context.Password))
            {
                context.Result = new GrantValidationResult(TokenRequestErrors.InvalidGrant, "invalid custom credential,username or pwd is null or whiteSpace ");
                return;
            }

            var isOK = await _userService.CheckUserAsync(context.UserName, context.Password);

            if (!isOK)
            {
                context.Result = new GrantValidationResult(TokenRequestErrors.InvalidGrant, "invalid custom credential,check error");
                return;
            }

            var user = await _userService.GetUserAsync(context.UserName);

            if (user == null)
            {
                throw new ArgumentException($"{context.UserName} exception");
            }

            context.Result = new GrantValidationResult(
            subject: context.UserName,  //用户名
            authenticationMethod: "custom",
            claims: new Claim[] //放入用户相关信息，不能放机密信息
            {
                    new Claim("Email",user.EmailAddress),
                    new Claim("UserId",user.Id.ToString()),
                    new Claim("RealName", user.Name),
            });
        }
    }
}
