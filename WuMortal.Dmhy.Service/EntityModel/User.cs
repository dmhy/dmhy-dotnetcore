﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WuMortal.Dmhy.Service.EntityModel
{
    public class User : BaseEntity
    {

        public string Name { get; set; }

        public string EmailAddress { get; set; }

        public string PasswordHash { get; set; }

        public string PasswordSalt { get; set; }

    }
}
