﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WuMortal.Dmhy.Service.EntityModel
{
    public class UserWebAPIRelation : BaseEntity
    {
        public long UserId { get; set; }

        public User User { get; set; }

        public long WebAPIId { get; set; }

        public WebAPI WebAPI { get; set; }
    }
}
