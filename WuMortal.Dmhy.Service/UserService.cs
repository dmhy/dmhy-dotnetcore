﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;
using WuMortal.Dmhy.Commons;
using WuMortal.Dmhy.Service.EntityModel;

namespace WuMortal.Dmhy.Service
{
    public class UserService : IServiceTag
    {
        public async Task<long> AddUserAsync(string name, string email, string password)
        {
            using (DmhyContext context = new DmhyContext())
            {
                BaseService<User> baseService = new BaseService<User>(context);

                var userEnity = await baseService.GetAll().SingleOrDefaultAsync(u => u.EmailAddress == email);
                if (userEnity != null)
                {
                    throw new ArgumentException($"邮箱地址:{email}以存在。");
                }

                string salt = UserHelper.Salt(6);
                var user = new User()
                {
                    Name = name,
                    EmailAddress = email,
                    PasswordSalt = salt,
                    PasswordHash = UserHelper.Md5(password + salt)
                };

                context.Users.Add(user);

                await context.SaveChangesAsync();

                return user.Id;
            }
        }

        /// <summary>
        /// 根据Id获取用户
        /// </summary>
        /// <param name="id">用户Id</param>
        /// <returns></returns>
        public async Task<User> GetUserAsync(long id)
        {
            using (DmhyContext context = new DmhyContext())
            {
                BaseService<User> baseService = new BaseService<User>(context);

                var userEnity = await baseService.GetAll().AsNoTracking()
                    .SingleOrDefaultAsync(u => u.Id == id);
                if (userEnity == null)
                {
                    throw new ArgumentException($"Id:{id}不存在。");
                }

                return userEnity;
            }
        }

        /// <summary>
        /// 获取所有用户
        /// </summary>
        /// <returns></returns>
        public async Task<User[]> GetAllUserAsync()
        {
            using (DmhyContext context = new DmhyContext())
            {
                BaseService<User> baseService = new BaseService<User>(context);

                return await baseService.GetAll()
                    .AsNoTracking().ToArrayAsync();
            }
        }

        /// <summary>
        /// 根据邮箱地址获取用户
        /// </summary>
        /// <param name="email">邮箱地址</param>
        /// <returns></returns>
        public async Task<User> GetUserAsync(string email)
        {
            using (DmhyContext context = new DmhyContext())
            {
                BaseService<User> baseService = new BaseService<User>(context);

                var userEnity = await baseService.GetAll().AsNoTracking()
                    .SingleOrDefaultAsync(u => u.EmailAddress == email);
                if (userEnity == null)
                {
                    throw new ArgumentException($"邮箱地址:{email}不以存在。");
                }

                return userEnity;
            }
        }

        /// <summary>
        /// 检查用户
        /// </summary>
        /// <param name="email">邮箱地址</param>
        /// <param name="pwd">密码</param>
        /// <returns></returns>
        public async Task<bool> CheckUserAsync(string email, string pwd)
        {
            using (DmhyContext context = new DmhyContext())
            {
                BaseService<User> baseService = new BaseService<User>(context);

                var userEnity = await baseService.GetAll().AsNoTracking()
                    .SingleOrDefaultAsync(u => u.EmailAddress == email);

                if (userEnity == null)
                {
                    return false;
                }

                string pwdHash = UserHelper.Md5(pwd + userEnity.PasswordSalt);
                if (userEnity.PasswordHash != pwdHash)
                {
                    return false;
                }

                return true;
            }
        }

        public async Task DeletedUserAsync(long id)
        {
            using (DmhyContext context = new DmhyContext())
            {
                BaseService<User> baseService = new BaseService<User>(context);

                var userEnity = await baseService.GetAll()
                    .SingleOrDefaultAsync(u => u.Id == id);
                if (userEnity == null)
                {
                    throw new ArgumentException($"Id:{id}不存在。");
                }

                await baseService.MakeDeleteAsync(id);
            }
        }
    }
}
