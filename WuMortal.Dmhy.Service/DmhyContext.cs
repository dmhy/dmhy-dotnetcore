﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using WuMortal.Dmhy.Service.EntityModel;

namespace WuMortal.Dmhy.Service
{
    public class DmhyContext : DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);

            //获取连接字符串
            var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory())
                                .AddJsonFile("appsettings.json").Build();
            var connstring = builder.GetSection("ConnectionStrings:DmhyDatabase").Value;

            optionsBuilder.UseMySql(connstring);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            //加载FluentAPI
            Assembly asmServices = Assembly.Load(new AssemblyName("WuMortal.Dmhy.Service"));
            modelBuilder.ApplyConfigurationsFromAssembly(asmServices);
        }

        public DbSet<User> Users { get; set; }

        public DbSet<AdminUser> AdminUsers { get; set; }

        public DbSet<WebAPI> WebAPIs { get; set; }

        public DbSet<UserWebAPIRelation> UserWebAPIRelations { get; set; }
    }
}
