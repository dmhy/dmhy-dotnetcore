﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WuMortal.Dmhy.Service.EntityModel;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace WuMortal.Dmhy.Service
{
    public class WebAPIService : IServiceTag
    {
        /// <summary>
        /// 添加WebAPI信息
        /// </summary>
        /// <param name="name">API名称</param>
        /// <param name="descible">描述</param>
        /// <returns></returns>
        public async Task<long> AddAPIAsync(string name, string descible)
        {
            using (DmhyContext context = new DmhyContext())
            {

                var webAPI = await context.WebAPIs.SingleOrDefaultAsync(w => w.Name == name);

                if (webAPI != null)
                {
                    throw new ArgumentException($"API: {name} 以存在。");
                }

                var api = new WebAPI()
                {
                    Name = name,
                    Descible = descible
                };

                context.WebAPIs.Add(api);

                await context.SaveChangesAsync();

                return api.Id;
            }
        }

        public async Task<WebAPI> GetWebAPIAsync(long id)
        {
            using (DmhyContext context = new DmhyContext())
            {
                return await context.WebAPIs
                    .AsNoTracking().SingleOrDefaultAsync(w => w.Id == id);
            }
        }

        public async Task<WebAPI> GetWebAPIAsync(string name)
        {
            using (DmhyContext context = new DmhyContext())
            {
                return await context.WebAPIs
                    .AsNoTracking().SingleOrDefaultAsync(w => w.Name == name);
            }
        }

        /// <summary>
        /// 用户的所有API权限
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<WebAPI[]> GetWebAPIByUserIdAsync(long id)
        {
            using (DmhyContext context = new DmhyContext())
            {
                var userWebAPIS = await context.UserWebAPIRelations.Where(u => u.UserId == id)
                    .Include(u => u.WebAPI).ToListAsync();

                WebAPI[] apis = new WebAPI[userWebAPIS.Count];

                for (int i = 0; i < userWebAPIS.Count; i++)
                {
                    apis[i] = new WebAPI
                    {
                        Name = userWebAPIS[i].WebAPI.Name,
                        Id = userWebAPIS[i].Id,
                        Descible = userWebAPIS[i].WebAPI.Descible,
                        IsDeleted = userWebAPIS[i].IsDeleted
                    };
                }

                return apis;
            }
        }

        /// <summary>
        /// 设置用户API状态
        /// </summary>
        /// <param name="id">记录Id</param>
        /// <param name="userId">用户ID</param>
        /// <param name="status">状态</param>
        /// <returns></returns>
        public async Task SetUserWebAPIStatusAsync(long id, long userId, bool status)
        {
            using (DmhyContext context = new DmhyContext())
            {
                var user = await context.Users.SingleOrDefaultAsync(u => u.Id == userId);

                if (user == null)
                {
                    throw new ArgumentException($"用户Id为：{userId} 不存在");
                }

                var userWebAPIRelations = await context.UserWebAPIRelations
                    .SingleOrDefaultAsync(u => u.Id == id);

                if (userWebAPIRelations.UserId != userId)
                {
                    throw new ArgumentException($"用户Id为：{userId} 数据错误");
                }

                userWebAPIRelations.IsDeleted = status;

                await context.SaveChangesAsync();

            }
        }

        public async Task SetWebAPIStatusAsync(long id, bool status)
        {
            using (DmhyContext context = new DmhyContext())
            {
                var userWebAPI = await context.WebAPIs
                    .SingleOrDefaultAsync(u => u.Id == id);

                if (userWebAPI == null)
                {
                    throw new ArgumentException($"WebAPI ID：{id} 不存在");
                }

                userWebAPI.IsDeleted = status;

                await context.SaveChangesAsync();

            }
        }

        /// <summary>
        /// 为用户添加API
        /// </summary>
        /// <param name="userId">用户Id</param>
        /// <param name="ids">apiIds</param>
        /// <returns></returns>
        public async Task AddUserAPIAsync(long userId, long[] ids)
        {
            using (DmhyContext context = new DmhyContext())
            {
                var user = await context.Users.SingleOrDefaultAsync(u => u.Id == userId);

                if (user == null)
                {
                    throw new ArgumentException($"用户Id为：{userId} 不存在");
                }

                //获取API
                var userWebAPIS = context.WebAPIs.Where(u => ids.Contains(u.Id));

                foreach (var api in userWebAPIS)
                {
                    context.UserWebAPIRelations.Add(new UserWebAPIRelation
                    {
                        UserId = userId,
                        WebAPIId = api.Id
                    });
                }

                await context.SaveChangesAsync();
            }
        }

        /// <summary>
        /// 更新用户API
        /// </summary>
        /// <param name="userId">用户Id</param>
        /// <param name="ids">apiIds</param>
        /// <returns></returns>
        public async Task UpdateUserAPIAsync(long userId, long[] ids)
        {
            using (DmhyContext context = new DmhyContext())
            {
                var user = await context.Users.SingleOrDefaultAsync(u => u.Id == userId);

                if (user == null)
                {
                    throw new ArgumentException($"用户Id为：{userId} 不存在");
                }

                //获取API
                var webAPIS = context.WebAPIs.Where(u => ids.Contains(u.Id));

                //清除用户原有API
                var userAPIs = context.UserWebAPIRelations.Where(u => u.Id == userId);
                context.UserWebAPIRelations.RemoveRange(userAPIs);

                await context.SaveChangesAsync();

                //添加新的API
                foreach (var api in webAPIS)
                {
                    context.UserWebAPIRelations.Add(new UserWebAPIRelation
                    {
                        UserId = userId,
                        WebAPIId = api.Id
                    });
                }

                await context.SaveChangesAsync();
            }
        }

        public async Task<WebAPI[]> GetAllWebAPIsAsync()
        {
            using (DmhyContext context = new DmhyContext())
            {
                return await context.WebAPIs
                    .AsNoTracking().ToArrayAsync();
            }
        }

        /// <summary>
        /// 修改WebAPI信息
        /// </summary>
        /// <returns></returns>
        public async Task UpdateAPIAsync(WebAPI webAPI)
        {
            using (DmhyContext context = new DmhyContext())
            {

                var webAPIEntity = await context.WebAPIs.SingleOrDefaultAsync(w => w.Id == webAPI.Id);

                if (webAPIEntity == null)
                {
                    throw new ArgumentException($"API: {webAPI.Id} 不存在。");
                }

                webAPIEntity.Name = webAPI.Name;
                webAPIEntity.Descible = webAPI.Descible;

                await context.SaveChangesAsync();
            }
        }

        public async Task DeletedAsync(long id)
        {
            using (DmhyContext context = new DmhyContext())
            {

                var webAPI = await context.WebAPIs.SingleOrDefaultAsync(w => w.Id == id);

                if (webAPI == null)
                {
                    throw new ArgumentException($"API: {id} 不存在。");
                }

                webAPI.IsDeleted = true;

                await context.SaveChangesAsync();
            }
        }
    }
}
