﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using WuMortal.Dmhy.Service.EntityModel;
using System.Threading.Tasks;

namespace WuMortal.Dmhy.Service
{
    public class BaseService<T> 
        where T : BaseEntity
    {
        private DmhyContext _dmhyContext;

        public BaseService(DmhyContext context)
        {
            _dmhyContext = context;
        }

        public IQueryable<T> GetAll()
        {
            return _dmhyContext.Set<T>().Where(e => !e.IsDeleted);
        }

        public T GetById(long id)
        {
            return GetAll().Where(e => e.Id == id).SingleOrDefault();
        }

        /// <summary>
        /// 软删除
        /// </summary>
        /// <param name="id"></param>
        public async Task MakeDeleteAsync(long id)
        {
            var data = GetById(id);
            if (data != null)
            {
                data.IsDeleted = true;
            }
            await _dmhyContext.SaveChangesAsync();
        }
    }
}
