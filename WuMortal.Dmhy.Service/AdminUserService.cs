﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;
using WuMortal.Dmhy.Commons;
using WuMortal.Dmhy.Service.EntityModel;

namespace WuMortal.Dmhy.Service
{
    public class AdminUserService : IServiceTag
    {
        public async Task<long> AddAdminUserAsync(string name, string email, string password)
        {
            using (DmhyContext context = new DmhyContext())
            {
                BaseService<AdminUser> baseService = new BaseService<AdminUser>(context);

                var adminUserEnity = await baseService.GetAll().SingleOrDefaultAsync(u => u.EmailAddress == email);
                if (adminUserEnity != null)
                {
                    throw new ArgumentException($"邮箱地址:{email}以存在。");
                }

                string salt = UserHelper.Salt(6);
                var AdminUser = new AdminUser()
                {
                    Name = name,
                    EmailAddress = email,
                    PasswordSalt = salt,
                    PasswordHash = UserHelper.Md5(password + salt)
                };

                context.AdminUsers.Add(AdminUser);

                await context.SaveChangesAsync();

                return AdminUser.Id;
            }
        }

        /// <summary>
        /// 根据Id获取用户
        /// </summary>
        /// <param name="id">用户Id</param>
        /// <returns></returns>
        public async Task<AdminUser> GetAdminUserAsync(long id)
        {
            using (DmhyContext context = new DmhyContext())
            {
                BaseService<AdminUser> baseService = new BaseService<AdminUser>(context);

                var adminUserEnity = await baseService.GetAll().AsNoTracking()
                    .SingleOrDefaultAsync(u => u.Id == id);
                if (adminUserEnity == null)
                {
                    throw new ArgumentException($"Id:{id}不存在。");
                }

                return adminUserEnity;
            }
        }

        /// <summary>
        /// 根据邮箱地址获取用户
        /// </summary>
        /// <param name="email">邮箱地址</param>
        /// <returns></returns>
        public async Task<AdminUser> GetAdminUserAsync(string email)
        {
            using (DmhyContext context = new DmhyContext())
            {
                BaseService<AdminUser> baseService = new BaseService<AdminUser>(context);

                var adminUserEnity = await baseService.GetAll().AsNoTracking()
                    .SingleOrDefaultAsync(u => u.EmailAddress == email);
                if (adminUserEnity == null)
                {
                    throw new ArgumentException($"邮箱地址:{email}不存在。");
                }

                return adminUserEnity;
            }
        }

        /// <summary>
        /// 检查用户
        /// </summary>
        /// <param name="email">邮箱地址</param>
        /// <param name="pwd">密码</param>
        /// <returns></returns>
        public async Task<bool> CheckAdminUserAsync(string email, string pwd)
        {
            using (DmhyContext context = new DmhyContext())
            {
                BaseService<AdminUser> baseService = new BaseService<AdminUser>(context);

                var adminUserEnity = await baseService.GetAll().AsNoTracking()
                    .SingleOrDefaultAsync(u => u.EmailAddress == email);

                if (adminUserEnity == null)
                {
                    return false;
                }

                string pwdHash = UserHelper.Md5(pwd + adminUserEnity.PasswordSalt);
                if (adminUserEnity.PasswordHash != pwdHash)
                {
                    return false;
                }

                return true;
            }
        }
    }
}
