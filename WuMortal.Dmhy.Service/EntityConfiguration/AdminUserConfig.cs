﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WuMortal.Dmhy.Service.EntityModel;

namespace WuMortal.Dmhy.Service.EntityConfiguration
{
    public class AdminUserConfig : IEntityTypeConfiguration<AdminUser>
    {
        public void Configure(EntityTypeBuilder<AdminUser> builder)
        {
            builder.ToTable("T_AdminUsers");

            builder.Property(u => u.Name).HasMaxLength(32).IsRequired();
            builder.Property(u => u.EmailAddress).HasMaxLength(64).IsRequired();
            builder.Property(u => u.PasswordHash).HasMaxLength(128).IsRequired().IsUnicode(false);
            builder.Property(u => u.PasswordSalt).HasMaxLength(16).IsRequired().IsUnicode(false);
        }
    }
}
