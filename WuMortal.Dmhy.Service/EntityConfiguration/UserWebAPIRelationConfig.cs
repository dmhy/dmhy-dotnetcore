﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using WuMortal.Dmhy.Service.EntityModel;

namespace WuMortal.Dmhy.Service.EntityConfiguration
{
    public class UserWebAPIRelationConfig : IEntityTypeConfiguration<UserWebAPIRelation>
    {
        public void Configure(EntityTypeBuilder<UserWebAPIRelation> builder)
        {
            builder.ToTable("T_UserWebAPIRelation");

            builder.HasOne(u => u.User).WithMany().HasForeignKey(u => u.UserId).IsRequired();
            builder.HasOne(u => u.WebAPI).WithMany().HasForeignKey(u => u.WebAPIId).IsRequired();
        }
    }
}
