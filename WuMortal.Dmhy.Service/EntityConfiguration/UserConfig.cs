﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WuMortal.Dmhy.Service.EntityModel;

namespace WuMortal.Dmhy.Service.EntityConfiguration
{
    public class UserConfig : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.ToTable("T_Users");

            builder.Property(u => u.Name).HasMaxLength(32).IsRequired();
            builder.Property(u => u.EmailAddress).HasMaxLength(64).IsRequired();
            builder.Property(u => u.PasswordHash).HasMaxLength(128).IsRequired().IsUnicode(false);
            builder.Property(u => u.PasswordSalt).HasMaxLength(16).IsRequired().IsUnicode(false);
        }
    }
}
