﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WuMortal.Dmhy.Service.EntityModel;

namespace WuMortal.Dmhy.Service.EntityConfiguration
{
    public class WebAPIConfig : IEntityTypeConfiguration<WebAPI>
    {
        public void Configure(EntityTypeBuilder<WebAPI> builder)
        {
            builder.ToTable("T_WebAPIs");

            builder.Property(w=>w.Name).HasMaxLength(64).IsRequired();
            builder.Property(w=>w.Descible).HasMaxLength(64).IsRequired();
        }
    }
}
